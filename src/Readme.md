# Steps to pass data from parent to child

1. Declare Variable that holds the actual value of the variable in the parent component

2. Include the child component selector in the html of the parent component

3. create variable having @input() decorator in the child component

These variables will be used as a way to pass data from parent to the child component

# Example

 <parent-compoent>
      <child-component>
   </child-component>
 </parent-component>

This is how child-component is called inside another component.
Now let's passs bookTitle from the parent and display it in the child-component

To do so

1. Create variable in the child component that accepts the value from the parent
   This variable has to be decorated as @input()
   @bookTitle() This means it is ready to accept value from any component that renders the child-component
   in the name #bookTitle
2. Now, let's modify the html of the parent component. The child component will have one more attribute which is bookTitlen
   <parent-component>
   <child-component bookTitle="introduction to angular" >
   </child-component>
   </parent-component

The parent component has forwarded value for the bookTitle and now its time to render it inside the parent component
<code>
{{bookTitle}}
</code>
