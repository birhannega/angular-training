import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css'],
})
export class ChildComponent implements OnInit {
  @Input() fullName?: string;
  @Input() gender = '';
  @Input() title = '';

  constructor() {}

  ngOnInit(): void {}
}
