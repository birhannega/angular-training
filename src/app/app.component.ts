import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'TimeCard';
  nameFromParent = 'Brook Tarekegn';
  genderFromParent = 'Female';
  booklists: any[] = [
    { title: 'learn google cloud', price: 30 },
    { title: 'Learn amazon cloud', price: 30 },
    { title: 'Learn react component', price: 30 },
    { title: 'Nodejs Guru ', price: 30 },
    { title: 'java cloud', price: 30 },
    { title: 'Aws service', price: 25 },
  ];
}

interface Person {
  fullName: string;
  gender: string;
}
